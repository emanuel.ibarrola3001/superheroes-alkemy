import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Auth, ResponseAuth } from '@Core/Models/auth';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  urlApi = environment.urlApi;

  constructor(private http: HttpClient) {}

  loginService(value: Auth): Observable<ResponseAuth> {
    return this.http.post<ResponseAuth>(this.urlApi, value);
  }
}
