import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '@Modules/auth/services/login.service';
import { StoreService } from '@Shared/service/store.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth-pages',
  templateUrl: './auth-pages.component.html',
  styleUrls: ['./auth-pages.component.css'],
})
export class AuthPagesComponent implements OnInit {
  formLogin: FormGroup = new FormGroup({});

  email = 'challenge@alkemy.org';
  password = 'angular';

  constructor(
    private login: LoginService,
    private router: Router,
    private store: StoreService,
    private toastr: ToastrService
  ) {}
  emailValidator = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

  ngOnInit(): void {
    this.formLogin = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.emailValidator),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(12),
      ]),
    });
  }

  sendLogin() {
    const form = this.formLogin.value;
    if (form.email == this.email && form.password == this.password) {
      let token = '20220311175523';
      this.SavenToken(token);
      console.log('exitos');
    } else {
      this.toastr.error('ERROR EMAIL O PASSWORD INCORRECTOS', 'ERROR LOGIN');
    }
  }

  showErrorInput() {
    this.formLogin.controls['email'].setValue('error');
  }

  SavenToken(objToken: any) {
    this.store.setStore('token', objToken.token);
    this.router.navigate(['/']);
  }
}
