import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetallePagesComponent } from './Pages/detalle-pages/detalle-pages.component';

const routes: Routes = [{ path: '', component: DetallePagesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleHeroeRoutingModule {}
