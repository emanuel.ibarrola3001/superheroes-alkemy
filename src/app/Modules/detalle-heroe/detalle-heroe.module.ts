import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetalleHeroeRoutingModule } from './detalle-heroe-routing.module';
import { DetallePagesComponent } from './Pages/detalle-pages/detalle-pages.component';
import { SharedModule } from '@Shared/shared.module';

@NgModule({
  declarations: [DetallePagesComponent],
  imports: [CommonModule, DetalleHeroeRoutingModule, SharedModule],
})
export class DetalleHeroeModule {}
