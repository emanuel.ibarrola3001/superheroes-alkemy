import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallePagesComponent } from './detalle-pages.component';

describe('DetallePagesComponent', () => {
  let component: DetallePagesComponent;
  let fixture: ComponentFixture<DetallePagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallePagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallePagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
