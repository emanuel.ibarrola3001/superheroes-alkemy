import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataSuperService } from '@Shared/service/data-super.service';

@Component({
  selector: 'app-detalle-pages',
  templateUrl: './detalle-pages.component.html',
  styleUrls: ['./detalle-pages.component.css'],
})
export class DetallePagesComponent implements OnInit {
  numid: any = '';
  heroe: any;
  ojos: string = '';
  pelo: string = '';

  constructor(
    private route: ActivatedRoute,
    private dataSuper: DataSuperService
  ) {}

  ngOnInit(): void {
    this.numid = this.route.snapshot.params['id'];
    this.heroeId(this.numid);
  }

  heroeId(id: string) {
    this.dataSuper.getSuperHeroeId(id).subscribe((data) => {
      this.heroe = data;
      let { 'eye-color': color, 'hair-color': pelo } = this.heroe.appearance;
      this.ojos = color;
      this.pelo = pelo;
    });
  }
}
