import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListHeroeRoutingModule } from './list-heroe-routing.module';
import { ListPagesComponent } from './Pages/list-pages/list-pages.component';
import { SharedModule } from '@Shared/shared.module';

@NgModule({
  declarations: [ListPagesComponent],
  imports: [CommonModule, SharedModule, ListHeroeRoutingModule],
})
export class ListHeroeModule {}
