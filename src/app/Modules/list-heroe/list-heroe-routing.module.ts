import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPagesComponent } from './Pages/list-pages/list-pages.component';

const routes: Routes = [{ path: '', component: ListPagesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListHeroeRoutingModule {}
