import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@Modules/list-heroe/list-heroe.module').then(
        (m) => m.ListHeroeModule
      ),
  },

  {
    path: 'detalle/:id',
    loadChildren: () =>
      import('@Modules/detalle-heroe/detalle-heroe.module').then(
        (m) => m.DetalleHeroeModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
