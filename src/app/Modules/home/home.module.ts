import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomePagesComponent } from './Pages/home-pages/home-pages.component';
import { SharedModule } from '@Shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomePagesComponent],
  imports: [CommonModule, HomeRoutingModule, SharedModule, FormsModule],
})
export class HomeModule {}
