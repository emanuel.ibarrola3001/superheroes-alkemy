import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DataSuperService {
  dataSuper = environment.urlApi + environment.tokenApi;

  heroes: BehaviorSubject<any> = new BehaviorSubject(null);
  teams: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {}

  getSuperHeroe(name: string): Observable<any> {
    const url = `${this.dataSuper}/search/${name}`;
    return this.http.get<any>(url);
  }

  getSuperHeroeId(id: string): Observable<any> {
    const url = `${this.dataSuper}/${id}`;

    return this.http.get<any>(url);
  }
}
