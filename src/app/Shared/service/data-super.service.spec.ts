import { TestBed } from '@angular/core/testing';

import { DataSuperService } from './data-super.service';

describe('DataSuperService', () => {
  let service: DataSuperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataSuperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
