import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';

import { SpinnerComponent } from './components/spinner/spinner.component';
import { GridHeroesComponent } from './components/grid-heroes/grid-heroes.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavbarComponent, SpinnerComponent, GridHeroesComponent],
  imports: [CommonModule, ReactiveFormsModule, RouterModule],
  exports: [NavbarComponent, SpinnerComponent, GridHeroesComponent],
})
export class SharedModule {}
