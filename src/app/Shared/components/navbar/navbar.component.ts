import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataSuperService } from '@Shared/service/data-super.service';
import { StoreService } from '@Shared/service/store.service';
import { debounceTime, filter, map, Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  nameSuper: string = '';
  spinner: boolean = false;
  Heroes!: Observable<any>;
  form!: FormGroup;

  constructor(
    private store: StoreService,
    private router: Router,
    private fb: FormBuilder,
    private dataSuper: DataSuperService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      buscador: [''],
    });
    this.form.valueChanges.pipe(debounceTime(500)).subscribe((current) => {
      const { buscador } = current;
      this.dataSuper.getSuperHeroe(buscador).subscribe(
        (dataHeroe) => {
          this.dataSuper.heroes.next(dataHeroe.results);
        },
        () => {
          console.log('Rompimos todo');
        }
      ),
        () => {
          this.toastr.error(
            'no se encontro ningun Personaje ',
            'Error Buscador'
          );
        };
    });
  }

  CerrarSesion() {
    this.router.navigate(['/login']);
    this.store.deletStore('token');
    console.log('cerrar login');
  }
}
