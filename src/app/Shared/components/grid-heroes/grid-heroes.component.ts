import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { DataSuperService } from '@Shared/service/data-super.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-grid-heroes',
  templateUrl: './grid-heroes.component.html',
  styleUrls: ['./grid-heroes.component.css'],
})
export class GridHeroesComponent implements OnInit {
  heroes!: any[];
  equipoHeroe!: any[];

  peso: number = 0;
  altura: number = 0;

  Intelligence: string = '';
  Strength: string = '';
  Speed: string = '';
  Durability: string = '';
  Power: string = '';
  Combat: string = '';

  constructor(
    private dataSuper: DataSuperService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.buscarHeroe();
    this.listarEquipo();
  }

  estadisticas() {
    this.PesoPromedio();
    this.AlturaPromedio();
    this.IntelligencePromedio();
    this.StrengthPromedio();
    this.SpeedPromedio();
    this.DurabilityPromedio();
    this.PowerPromedio();
    this.CombatPromedio();
  }

  buscarHeroe() {
    this.dataSuper.heroes.subscribe((data) => {
      this.heroes = data;
    });
  }

  add(heroe: any) {
    if (this.equipoHeroe) {
      if (this.HeroeRepetido(heroe)) {
        this.toastr.warning('no puede ingresar el heroe ', 'Repetido!');
      } else if (this.equipoHeroe.length > 5) {
        this.toastr.error(
          'No puede agregar mas Personajes a su equipo!',
          'ERROR EQUIPO!'
        );
      } else {
        this.dataSuper.teams.next([...this.equipoHeroe, heroe]);
        this.estadisticas();
        this.toastr.success('Personaje Agregado Exitosamente', 'Exito!');
      }
    } else {
      this.dataSuper.teams.next([heroe]);
      this.estadisticas();
      this.toastr.success('Personaje Agregado Exitosamente', 'Exito!');
    }
  }

  delete(heroe: any) {
    let indice = this.equipoHeroe.indexOf(heroe);
    if (indice >= 0) {
      this.equipoHeroe.splice(indice, 1);
      this.estadisticas();
      this.toastr.success(
        'Personaje Eliminado Correctamente',
        'Exito Eliminacion !'
      );
    }
  }

  listarEquipo() {
    this.dataSuper.teams.subscribe((dato) => {
      this.equipoHeroe = dato;
    });
  }

  info(id: number) {
    this.router.navigate([`detalle/${id}`]);
  }

  // metodos privados

  private HeroeRepetido(heroe: any): boolean {
    let valor = false;
    for (let index = 0; index < this.equipoHeroe.length; index++) {
      const element = this.equipoHeroe[index];
      if (heroe === element) {
        valor = true;

        return valor;
      }
    }

    return valor;
  }

  private PesoPromedio() {
    let pesos: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      const valorPeso = dat.appearance.weight[1].substr(0, 2);
      this.peso = parseInt(valorPeso);
      pesos.push(this.peso);
    });
    let suma = pesos.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / pesos.length;
    this.peso = Math.ceil(promedio);
  }

  private AlturaPromedio() {
    let alturas: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      const valorAltura = dat.appearance.height[1].substr(0, 3);
      this.altura = parseInt(valorAltura);
      alturas.push(this.altura);
    });
    let suma = alturas.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / alturas.length;
    this.altura = Math.ceil(promedio);
  }

  private IntelligencePromedio() {
    let inteli: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.intelligence.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        inteli.push(valorInteli);
      }
      if (valor === 'null') {
        inteli.push(0);
      }
    });
    let suma = inteli.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / inteli.length;
    promedio = Math.ceil(promedio);
    this.Intelligence = promedio.toString();
  }

  private StrengthPromedio() {
    let numArray: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.strength.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        numArray.push(valorInteli);
      }
      if (valor === 'null') {
        numArray.push(0);
      }
    });
    let suma = numArray.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / numArray.length;
    promedio = Math.ceil(promedio);
    this.Strength = promedio.toString();
  }
  private SpeedPromedio() {
    let numArray: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.speed.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        numArray.push(valorInteli);
      }
      if (valor === 'null') {
        numArray.push(0);
      }
    });
    let suma = numArray.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / numArray.length;
    promedio = Math.ceil(promedio);
    this.Speed = promedio.toString();
  }

  private DurabilityPromedio() {
    let numArray: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.durability.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        numArray.push(valorInteli);
      }
      if (valor === 'null') {
        numArray.push(0);
      }
    });
    let suma = numArray.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / numArray.length;
    promedio = Math.ceil(promedio);
    this.Durability = promedio.toString();
  }

  private PowerPromedio() {
    let numArray: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.power.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        numArray.push(valorInteli);
      }
      if (valor === 'null') {
        numArray.push(0);
      }
    });
    let suma = numArray.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / numArray.length;
    promedio = Math.ceil(promedio);
    this.Power = promedio.toString();
  }

  private CombatPromedio() {
    let numArray: number[] = [];
    this.equipoHeroe.forEach((dat) => {
      let valor = dat.powerstats.combat.substr(0, 4);
      if (valor !== 'null') {
        let valorInteli = parseInt(valor);
        numArray.push(valorInteli);
      }
      if (valor === 'null') {
        numArray.push(0);
      }
    });
    let suma = numArray.reduce((a, n) => ((a += n), a), 0);
    let promedio = suma / numArray.length;
    promedio = Math.ceil(promedio);
    this.Combat = promedio.toString();
  }
}
